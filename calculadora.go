package main

import (
	"math"
	"net/http"
)

type Calculadora struct{}

type Operacoes interface {
	Operacao(a, b float64) float64
}

// Criando struct para as operacoes
type Add struct{}
type Sub struct{}
type Mul struct{}
type Div struct{}
type Pow struct{}
type Rot struct{}

//Criando as funcoes para as operacoes

func (Add) Operacao(a, b float64) float64 {
	return a + b
}
func (Sub) Operacao(a, b float64) float64 {
	return a - b
}
func (Mul) Operacao(a, b float64) float64 {
	return a * b
}
func (Div) Operacao(a, b float64) float64 {
	return a / b
}
func (Pow) Operacao(a, b float64) float64 {
	return math.Pow(a, b)
}
func (Rot) Operacao(a, b float64) float64 {
	return math.Pow(a, (1 / b))
}

var OpMap = map[string]Operacoes{
	"add": Add{},
	"sub": Sub{},
	"mul": Mul{},
	"div": Div{},
	"pow": Pow{},
	"rot": Rot{},
}

func (c Calculadora) ExecOp(res http.ResponseWriter, action string, number1, number2 float64) (float64, error) {
	operacao, ok := OpMap[action]
	if !ok {
		SendInvalidOpResponse(res, "Operação inválida"+action)
		return 0, nil
	}
	resultado := operacao.Operacao(number1, number2)
	return resultado, nil
}
