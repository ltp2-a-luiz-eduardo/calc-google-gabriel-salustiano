package main

import (
	"log"
	"net/http"
	"time"
)

type ServerHTTP struct {
	Calculadora *Calculadora
	Parametros  *ParameterURL
}

func (s ServerHTTP) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	if req.Method != "GET" {
		SendInvalidPathResponse(res)
		return
	}
	action, Number1, Number2, err := s.Parametros.GetParameter(res, req)
	if err != nil {
		SendInvalidOpResponse(res, err.Error())
		return
	}
	result, err := s.Calculadora.ExecOp(res, action, Number1, Number2)
	if err != nil {
		SendInvalidOpResponse(res, err.Error())
		return
	}
	SendSuccessfullRes(res, result, action)
}
func StartServer() {
	Calculadora := &Calculadora{}
	Parameters := &ParameterURL{}
	s := &http.Server{
		Addr:         "localhost:8080",
		Handler:      ServerHTTP{Calculadora, Parameters},
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}
	log.Fatal(s.ListenAndServe())
}
