package main

import (
	"net/http"
	"strconv"
)

type ParameterURL struct{}

func (p *ParameterURL) GetParameter(res http.ResponseWriter, req *http.Request) (string, float64, float64, error) {
	params := req.URL.Query()

	action := params.Get("action")
	number1 := params.Get("number1")
	number2 := params.Get("number2")
	if action == "" || number1 == "" || number2 == "" {
		return "", 0, 0, nil
	}
	number1Float, err := strconv.ParseFloat(number1, 64)
	if err != nil {
		return "", 0, 0, nil
	}
	number2Float, err := strconv.ParseFloat(number2, 64)
	if err != nil {
		return "", 0, 0, nil
	}
	return action, number1Float, number2Float, nil
}
