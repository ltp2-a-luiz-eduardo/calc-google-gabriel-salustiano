package main

import (
	"encoding/json"
	"net/http"
)

func SendSuccessfullRes(res http.ResponseWriter, result float64, operacao string) {
	res.Header().Set("Content-Type", "application/json")
	response := map[string]interface{}{
		"code":   200,
		"result": result,
	}
	_, ok := OpMap[operacao]
	if !ok {
		return
	}
	response["op"] = operacao

	json.NewEncoder(res).Encode(response)
}

func SendInvalidOpResponse(res http.ResponseWriter, mensagem string) {
	res.WriteHeader(http.StatusBadRequest)
	response := map[string]interface{}{
		"code":   400,
		"result": "invalid expression",
		"op":     mensagem,
	}
	json.NewEncoder(res).Encode(response)
}

func SendInvalidPathResponse(res http.ResponseWriter) {
	res.WriteHeader(http.StatusNotFound)
	response := map[string]interface{}{
		"code":  404,
		"error": "Not Found.",
	}
	json.NewEncoder(res).Encode(response)
}

func SendErrorResponse(res http.ResponseWriter) {
	res.WriteHeader(http.StatusInternalServerError)
	response := map[string]interface{}{
		"code":  500,
		"error": "Something went wrong.",
	}
	json.NewEncoder(res).Encode(response)
}
